
***********
PyThesaurus
***********


Description
-----------



This package gets you the synonyms and definitions of an inputted word from the best dictionary sites available online. 

- `Thesaurus.com <http: www.thesaurus.com=""/>`_

- `Dictionary.com <http: www.dictionary.com=""/>`_


Why you need this package?
--------------------------


Though python provides lexical resources like WordNet, the variety of options it can provide will be poor when compares to the dictionary.com or the thesaurus.com. 

This will help the user to enhance their approaches when he/she is dealing with text mining, NLP techniques and much more.

How to install? 
---------------

Use basic pip install to install this library.  
::

  pip install py_thesaurus


How to use?
-----------


1. From python shell 
^^^^^^^^^^^^^^^^^^^^

::

   from py_thesaurus import WordAnalyzer

   input_word = "dream"

   new_instance = WordAnalyzer(input_word)

   print(new_instance.get_synonym())

   print(new_instance.get_definition())

2. From command line
^^^^^^^^^^^^^^^^^^^^

Positional arguments
::

  word --> Input word to get definition/synonym 


Optional arguments:
::

  -h or --help  Show this help message and exit
  -d            Option to fetch definitions
  -s            Option to fetch synonyms
  -a            Option to fetch both

Command
::

   py_thesaurus [-h] [-d] [-s] [-a] word


Contact
******************

Bitbucket: https://bitbucket.org/red_pills/py_thesaurus.git

email: sleebapaul@gmail.com, vaswin91@gmail.com
