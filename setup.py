from setuptools import setup, find_packages


with open('README.rst', encoding='utf-8') as file:
    long_description = file.read()

setup(name='py_thesaurus',
      version='0.95',
      description='To fetch the definitions and the synonyms of \
      an inputted word from thesaurus.com',
      long_description=long_description,
      classifiers=[
          'Development Status :: 3 - Alpha',
          'License :: OSI Approved :: MIT License',
          'Programming Language :: Python :: 3.5',
          'Topic :: Text Processing :: Linguistic'],
      url='https://sleebapaul@bitbucket.org/red_pills/py_thesaurus.git',
      keywords='nlp text-mining',
      author='red_pills',
      author_email='sleebapaul@gmail.com',
      license='MIT',
      packages=find_packages(exclude=['tests*', 'red_pill*']),
      install_requires=["bs4"],
      entry_points={"console_scripts": ['py_thesaurus=py_thesaurus.main:main']},
      zip_safe=False)

