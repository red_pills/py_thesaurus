"""Unit Testing"""
import unittest
from py_thesaurus import WordAnalyzer


class TestThesaurusMethodsForEmptyResponse(unittest.TestCase):
    """Test Case"""

    def setUp(self):
        """
         initialising with WordAnalyzer object
        """
        self.wordanalyser = WordAnalyzer('Sleeba')

    def test_get_synonym(self):
        """
        Check if a list of synonyms is returned for a word in vocabulary
        """
        self.assertFalse(self.wordanalyser.get_synonym())

    def test_get_definition(self):
        """
        Check if definition of the word can be retrieved from
        dictionary.compile
        """
        self.assertFalse(self.wordanalyser.get_definition())


class TestThesaurusMethods(unittest.TestCase):
    """Test Case"""

    def setUp(self):
        """
         initialising with WordAnalyzer object
        """
        self.wordanalyser = WordAnalyzer('jobs')

    def test_get_synonym(self):
        """
        Check if a list of synonyms is returned for a word in vocabulary
        """
        self.assertTrue(self.wordanalyser.get_synonym())

    def test_get_definition(self):
        """
        Check if definition of the word can be retrieved from
        dictionary.com
        """
        self.assertTrue(self.wordanalyser.get_definition())


def suite():
    """suite of tests"""
    suite1 = unittest.TestSuite()
    suite1.addTest(TestThesaurusMethodsForEmptyResponse())
    suite1.addTest(TestThesaurusMethods())
    return suite1
